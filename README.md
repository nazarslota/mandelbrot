# Mandelbrot Set

**Mandelbrot Set** is a very small but interesting project for me, because it uses Go which calls code written in C,
which was something new for me at that time.

**PS.** Thanks to my best friend for the idea of project =0.

## Run

In order to run this project, you need to install [Go](https://go.dev/dl/) and
the [Pixel](https://github.com/faiface/pixel) library (Be especially careful when installing C libraries).

## Usage

You can use the following keys to navigate through the set.

- **W** - up.
- **S** - down.
- **A** - left.
- **D** - right.
- **Left Shift** - increase the set.
- **Left Control** - reduce the set.

## Appearance

![Mandelbrot Set](assets/img1.png?raw=true)

![Mandelbrot Set](assets/img2.png?raw=true)
